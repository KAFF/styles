# Styles

Shared CSS between projects

```
<link rel="stylesheet" href="./styles/base.css" type="text/css">
<link rel="stylesheet" href="./styles/light.css" type="text/css" title="Light">
<link rel="alternate stylesheet" href="./styles/dark.css" type="text/css" title="Dark">
<link rel="alternate stylesheet" href="./styles/black.css" type="text/css" title="Black">

<link rel="stylesheet" href="./styles/print.css" type="text/css" media="print">
```

Default styles can be changed with a header, NGINX config:

```
# add_header Default-Style Light;
add_header Default-Style Dark;
```
